/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.quentin1.data;

/**
 *
 * @author student
 */
public enum LinkType {
    
    ISDN, ADSL, SDSL_ATM, SDSL_EFM, OPTICAL_FIBER, LEASED_LINE, DARK_FIBER
    
}
