/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.quentin1.data;

import java.util.Date;

/**
 *
 * @author student
 */
public class Link {
    
    private String label;
    private InstallationSite installationSite;
    private LinkType type;
    private ISP provider;
    private String providerId;
    private ISP carrier;
    private Date constructionDate;
    private Date activationDate;
    private Date resignationDate;
    private Date terminationDate;
    private Equipment[] equipments;
    
    public boolean isActive(){
        return true;
    }
}
