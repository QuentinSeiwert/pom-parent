/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.quentin1.services;

import java.util.ArrayList;
import net.tncy.quentin1.data.ISP;

/**
 *
 * @author student
 */
public class ISPService {
    
    private ArrayList<ISP> ISPList;
    
    public ISPService(){
        ISPList = new ArrayList<>();
    }
    
    public ISP create(String name){
        ISP newISP = new ISP(name);
        ISPList.add(newISP);
        return newISP;
    }
    
    public ArrayList<ISP> findAll(){
        return ISPList;
    }
    
    public ISP findByName(String name){
        for(ISP isp : ISPList){
            if(isp.getName().equals(name))
                return isp;
        }
        return null;
    }
    
    public void delete(ISP isp){
        ISPList.remove(isp);
    }
    
    public void update(ISP isp, String name){
        isp.setName(name);
    }
    
}
